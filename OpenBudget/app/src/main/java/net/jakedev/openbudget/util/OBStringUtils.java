package net.jakedev.openbudget.util;

/**
 * Created by jakelaurie on 1/04/16.
 */
public class OBStringUtils {
    public static String removeAllNonNumeric(String str) {
        return str.replaceAll("[^\\d.]", "");
    }
}

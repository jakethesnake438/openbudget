package net.jakedev.openbudget.di.component;

import android.content.Context;

import net.jakedev.openbudget.view.activity.BaseActivity;
import net.jakedev.openbudget.data.DataHelper;
import net.jakedev.openbudget.di.module.ApplicationModule;
import net.jakedev.openbudget.di.module.DataModule;
import net.jakedev.openbudget.view.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Jake Laurie on 8/02/16.
 */
@Singleton
@Component(modules = {ApplicationModule.class, DataModule.class})
public interface ApplicationComponent {
    void inject(BaseActivity baseActivity);
    void inject(MainActivity mainActivity);

    //Exposed to sub graphs
    Context context();
    DataHelper dataHelper();
}

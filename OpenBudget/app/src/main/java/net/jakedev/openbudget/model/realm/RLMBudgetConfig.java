package net.jakedev.openbudget.model.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Jake Laurie on 5/02/16.
 */
public class RLMBudgetConfig extends RealmObject {
    @PrimaryKey
    @Required
    private String currentBudgetID;

    public String getCurrentBudgetID() {
        return currentBudgetID;
    }

    public void setCurrentBudgetID(String currentBudgetID) {
        this.currentBudgetID = currentBudgetID;
    }
}

package net.jakedev.openbudget.view.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.jakedev.openbudget.R;
import net.jakedev.openbudget.di.component.BudgetComponent;
import net.jakedev.openbudget.model.BudgetDay;
import net.jakedev.openbudget.model.realm.IncomeExpenseWrapper;
import net.jakedev.openbudget.presenter.DailyBudgetPresenter;
import net.jakedev.openbudget.view.activity.MainActivity;
import net.jakedev.openbudget.view.adapter.BudgetExpenseAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jake Laurie on 2/02/16.
 */
public class DailyBudgetFragment extends BaseFragment implements DailyBudgetPresenter.View, AddExpenseDialogFragment.AddExpenseIncomeListener {

    @Bind(R.id.spent_text_view)
    TextView spentTextView;

    @Bind(R.id.total_text_view)
    TextView totalTextView;

    @Bind(R.id.budget_list_recycler)
    RecyclerView mRecyclerView;

    @Bind(R.id.bottom_plus_button)
    Button addIncomeButton;

    @Bind(R.id.bottom_minus_button)
    Button addExpenseButton;

    @Inject
    DailyBudgetPresenter mPresenter;

    BudgetExpenseAdapter mBudgetExpenseAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(BudgetComponent.class).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_daily_budget,
                container,
                false);

        ButterKnife.bind(this, v);
        ((MainActivity) getActivity()).hideActionbarTitle(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBudgetExpenseAdapter = new BudgetExpenseAdapter();
        mRecyclerView.setAdapter(mBudgetExpenseAdapter);
        mPresenter.setView(this);
        mPresenter.initialize();
        return v;
    }

    @OnClick(R.id.bottom_plus_button)
    public void addExpensePressed() {
        showAddExpenseDialogFragment(true);
    }

    @OnClick(R.id.bottom_minus_button)
    public void addIncomePressed() {
        showAddExpenseDialogFragment(false);
    }

    private void showAddExpenseDialogFragment(boolean isExpense) {
        AddExpenseDialogFragment dialogFragment =
                AddExpenseDialogFragment.newInstance(String.format("Enter %s detail",
                isExpense ? "expense" : "income"),
                isExpense);
        dialogFragment.setDialogListener(this);
        showFragment(dialogFragment);
    }

    @Override
    public void setBudget(BudgetDay budget) {
        spentTextView.setText(priceFormatDouble(budget.getSpent()));
        totalTextView.setText(String.format(getResources().getString(R.string.out_of_string),
                priceFormatDouble(budget.getRemaining())));
        mBudgetExpenseAdapter.setData(budget.getIncomeAndExpenseList());

    }

    public String priceFormatDouble(double price) {
        return String.format(mPresenter.getCurrencyCode() + "%.2f", price);
    }

    @Override
    public void dialogDismissed(boolean expenseOrIncomeAdded) {
        if(expenseOrIncomeAdded) {
            mPresenter.updateViewData();
        }
    }
}

package net.jakedev.openbudget.model;

import net.jakedev.openbudget.model.realm.IncomeExpenseWrapper;

import java.util.List;

/**
 * Created by jake on 24/02/16.
 */
public class BudgetDay {
    double spent, remaining;
    List<IncomeExpenseWrapper> incomeAndExpenseList;

    public BudgetDay(double spent,
                     double remaining,
                     List<IncomeExpenseWrapper> incomeAndExpenseList) {
        this.spent = spent;
        this.remaining = remaining;
        this.incomeAndExpenseList = incomeAndExpenseList;
    }

    public double getRemaining() {
        return remaining;
    }

    public double getSpent() {
        return spent;
    }

    public List<IncomeExpenseWrapper> getIncomeAndExpenseList() {
        return incomeAndExpenseList;
    }



}

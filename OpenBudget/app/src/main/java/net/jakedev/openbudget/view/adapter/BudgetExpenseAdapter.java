package net.jakedev.openbudget.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.jakedev.openbudget.R;
import net.jakedev.openbudget.model.realm.IncomeExpenseWrapper;

import java.util.List;
import java.util.Locale;

/**
 * Created by jake on 16/02/16.
 */
public class BudgetExpenseAdapter extends RecyclerView.Adapter {

    List<IncomeExpenseWrapper> mData;

    public void setData(List<IncomeExpenseWrapper> data){
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_budget_expense, parent, false);
        return new BudgetItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BudgetItemViewHolder vh = (BudgetItemViewHolder)holder;
        IncomeExpenseWrapper item = mData.get(position);

        vh.setPriceText(String.format(Locale.US, "%.2f", item.getPriceValue()));
        vh.setDescriptionText(item.getDescription());
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    private class BudgetItemViewHolder extends RecyclerView.ViewHolder {
        private TextView priceTextView;
        private TextView descriptionTextView;

        public BudgetItemViewHolder(View view) {
            super(view);
            this.priceTextView       = (TextView)view.findViewById(R.id.budget_list_item_price);
            this.descriptionTextView = (TextView)view.findViewById(R.id.budget_list_item_description);
        }

        public void setPriceText(String text) {
            priceTextView.setText(text);
        }

        public void setDescriptionText(String text) {
            descriptionTextView.setText(text);
        }
    }
}

package net.jakedev.openbudget.presenter;

import net.jakedev.openbudget.data.DataHelper;
import net.jakedev.openbudget.model.realm.RLMBudget;
import net.jakedev.openbudget.model.realm.RLMBudgetConfig;
import net.jakedev.openbudget.util.BudgetUtil;

import java.util.Date;

import javax.inject.Inject;

/**
 * Created by jake on 22/02/16.
 */
public class BudgetActivityPresenter extends Presenter {

    public interface View {
        void noBudgetAvailable();
        void budgetAvailable();
    }

    private DataHelper mDataHelper;
    private View mView;

    @Inject
    public BudgetActivityPresenter(DataHelper dataHelper) {
        mDataHelper = dataHelper;
    }

    public void setView(View view) {
        mView = view;
    }

    public void initialize() {
        checkBudgetExistsForCurrentDate();
    }

    private void checkBudgetExistsForCurrentDate() {
        RLMBudgetConfig config = mDataHelper.getCurrentConfig();
        Date today = BudgetUtil.getTodaysDateTimeless();
        if(config == null) {
            mView.noBudgetAvailable();
        } else {
            RLMBudget RLMBudget = mDataHelper.getBudget();
            //Check RLMBudget is valid
            if(RLMBudget != null && (RLMBudget.getStartDate().getTime() <= today.getTime()
                    || today.getTime() < RLMBudget.getEndDate().getTime())) { // TODO: check this logic
                mView.budgetAvailable();
            } else {
                mView.noBudgetAvailable();
            }
        }
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}

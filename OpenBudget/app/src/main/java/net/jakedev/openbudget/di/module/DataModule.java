package net.jakedev.openbudget.di.module;

import android.content.Context;

import net.jakedev.openbudget.data.DataHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jake on 15/02/16.
 */
@Module
public class DataModule {

    private final Context mCtx;

    public DataModule(Context ctx) {
        mCtx = ctx;
    }

    @Provides @Singleton DataHelper provideDataHelper() {
        return new DataHelper(mCtx);
    }
}

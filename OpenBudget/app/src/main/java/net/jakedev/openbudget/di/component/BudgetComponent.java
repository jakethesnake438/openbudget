package net.jakedev.openbudget.di.component;

import net.jakedev.openbudget.di.module.ActivityModule;
import net.jakedev.openbudget.di.module.BudgetModule;
import net.jakedev.openbudget.di.PerActivity;
import net.jakedev.openbudget.presenter.AddExpensePresenter;
import net.jakedev.openbudget.view.activity.MainActivity;
import net.jakedev.openbudget.view.fragment.AddExpenseDialogFragment;
import net.jakedev.openbudget.view.fragment.DailyBudgetFragment;
import net.jakedev.openbudget.view.fragment.EnterDetailFragment;

import dagger.Component;

/**
 * Created by Jake Laurie on 8/02/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, BudgetModule.class})
public interface BudgetComponent extends ActivityComponent {
    void inject(DailyBudgetFragment dailyBudgetFragment);
    void inject(EnterDetailFragment enterDetailFragment);
    void inject(AddExpensePresenter addExpensePresenter);
    void inject(AddExpenseDialogFragment addExpenseDialogFragment);
    void inject(MainActivity mainActivity);

}

package net.jakedev.openbudget.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import net.jakedev.openbudget.view.activity.MainActivity;
import net.jakedev.openbudget.R;
import net.jakedev.openbudget.di.component.BudgetComponent;
import net.jakedev.openbudget.presenter.EnterDetailPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;

/**
 * Created by Jake Laurie on 31/01/16.
 */

public class EnterDetailFragment extends BaseFragment {

    @Bind(R.id.period_type_spinner) public Spinner budgetPeriodTypeSpinner;
    @Bind(R.id.period_type_spinner_currency) public Spinner budgetCurrencySpinner;
    @Bind(R.id.apply_button) public Button applyButton;
    @Bind(R.id.enter_period_text) EditText periodText;
    @Bind(R.id.enter_credit_text) EditText creditText;

    @Inject
    EnterDetailPresenter mPresenter;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(BudgetComponent.class).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_enter_detail,
                container,
                false);
        setupView(v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(savedInstanceState == null) {

        }
        setupSpinners();
    }

    private void setupSpinners() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,
                mPresenter.getPeriodSpinnerValues());
        budgetPeriodTypeSpinner.setAdapter(adapter);

        ArrayAdapter<String> adapterCurrency = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,
                mPresenter.getCurrencyCodeSpinnerValues());
        budgetCurrencySpinner.setAdapter(adapterCurrency);

    }

    @OnTextChanged({R.id.enter_period_text, R.id.enter_credit_text})
    @OnFocusChange({R.id.enter_period_text, R.id.enter_credit_text})
    public void focusChanged() {
        checkShowApplyButton();
    }

    @OnClick(R.id.apply_button)
    public void applyPressed() {
        mPresenter.setupBudgetInformation(periodText.getText().toString(),
                creditText.getText().toString(),
                budgetPeriodTypeSpinner.getSelectedItemPosition());

        ((MainActivity) getActivity()).presentDailyBudgetView();
    }

    private void setupView(View v) {
        ButterKnife.bind(this, v);
    }

    private void checkShowApplyButton() {
        boolean shouldShow = mPresenter.validate(periodText.getText().toString(),
                creditText.getText().toString());
        applyButton.setTextColor(shouldShow
                ? Color.BLACK
                : Color.GRAY);
        applyButton.setEnabled(shouldShow);
    }

    @Override
    public boolean handlesBackPress() {
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        hideToolbar(false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

package net.jakedev.openbudget.di.module;

import android.app.Activity;

import net.jakedev.openbudget.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Jake Laurie on 8/02/16.
 */
@Module
public class ActivityModule {
    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides @PerActivity
    Activity activity() {
        return this.activity;
    }
}
package net.jakedev.openbudget.di.component;

import android.app.Activity;

import net.jakedev.openbudget.di.module.ActivityModule;
import net.jakedev.openbudget.di.PerActivity;
import net.jakedev.openbudget.di.module.BudgetModule;
import net.jakedev.openbudget.view.activity.MainActivity;
import net.jakedev.openbudget.view.fragment.EnterDetailFragment;

import dagger.Component;

/**
 * Created by Jake Laurie on 8/02/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, BudgetModule.class})
public interface ActivityComponent {
    //Exposed to sub-graphs
    Activity activity();

}

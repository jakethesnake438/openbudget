package net.jakedev.openbudget.model.realm;

import io.realm.RealmObject;

/**
 * Created by jake on 16/02/16.
 */
public class RLMExpense extends RealmObject {

    private double priceValue;
    private String description;

    /**
     * Creates the RLMIncome Object
     * @param priceValue Must be set with a positive value
     * @param description Any string value
     */
    public RLMExpense(double priceValue, String description) {
        this.priceValue = priceValue;
        this.description = description;
    }

    public RLMExpense() {}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(double priceValue) {
        this.priceValue = priceValue;
    }
}

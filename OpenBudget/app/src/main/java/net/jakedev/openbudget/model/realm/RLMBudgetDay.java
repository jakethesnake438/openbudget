package net.jakedev.openbudget.model.realm;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Jake Laurie on 5/02/16.
 */
public class RLMBudgetDay extends RealmObject {
    @PrimaryKey
    private long dayLong;
    private Date day;

    @Required
    private String budgetID;
    private double budgetAmount;
    private double budgetSpent;
    private RealmList<RLMExpense> RLMExpenseList;
    private RealmList<RLMIncome> RLMIncomeList;

    public void setRLMExpenseList(RealmList<RLMExpense> RLMExpenseList) {
        this.RLMExpenseList = RLMExpenseList;
    }

    public RealmList<RLMExpense> getRLMExpenseList() {
        if(RLMExpenseList == null) {
            RLMExpenseList = new RealmList<>();
        }
        return RLMExpenseList;
    }

    public void setRLMIncomeList(RealmList<RLMIncome> RLMIncomeList) {
        this.RLMIncomeList = RLMIncomeList;
    }

    public RealmList<RLMIncome> getRLMIncomeList() {
        if(RLMIncomeList == null) {
            RLMIncomeList = new RealmList<>();
        }
        return RLMIncomeList;
    }


    public long getDayLong() {
        return dayLong;
    }

    public void setDayLong(long mDayLong) {
        this.dayLong = mDayLong;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date mDay) {
        this.dayLong = mDay.getTime();
        this.day = mDay;
    }

    public double getBudgetAmount() {
        return budgetAmount;
    }

    public void setBudgetAmount(double mBudgetAmount) {
        this.budgetAmount = mBudgetAmount;
    }

    public double getBudgetSpent() {
        return budgetSpent;
    }

    public void setBudgetSpent(double mBudgetSpent) {
        this.budgetSpent = mBudgetSpent;
    }

    public String getBudgetID() {
        return budgetID;
    }

    public void setBudgetID(String mBudgetID) {
        this.budgetID = mBudgetID;
    }
}

package net.jakedev.openbudget.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import net.jakedev.openbudget.R;
import net.jakedev.openbudget.view.activity.BaseActivity;
import net.jakedev.openbudget.view.activity.MainActivity;
import net.jakedev.openbudget.di.HasComponent;

/**
 * Created by jake on 31/01/16.
 */
public class BaseFragment extends Fragment implements MainActivity.OnBackPressedHandler {

    @Override
    public boolean handlesBackPress() {
        return false;
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((BaseActivity)getActivity()).setSelectedFragment(this);
    }

    protected void hideToolbar(boolean hide) {
       getToolbar().setVisibility(hide ? View.GONE : View.VISIBLE);
    }

    protected Toolbar getToolbar() {
        return ((MainActivity) getActivity()).mToolbar;
    }

    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    protected void replaceFragment(Fragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, fragment)
                .addToBackStack(fragment.getClass().getSimpleName()).commit();
    }

    protected void showFragment(DialogFragment fragment) {
        FragmentManager fm = getFragmentManager();
        fragment.show(fm, fm.getClass().getSimpleName());
    }

}

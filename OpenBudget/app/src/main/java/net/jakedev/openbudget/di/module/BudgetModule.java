package net.jakedev.openbudget.di.module;

import android.content.Context;

import net.jakedev.openbudget.data.DataHelper;
import net.jakedev.openbudget.di.PerActivity;
import net.jakedev.openbudget.presenter.AddExpensePresenter;
import net.jakedev.openbudget.presenter.BudgetActivityPresenter;
import net.jakedev.openbudget.presenter.DailyBudgetPresenter;
import net.jakedev.openbudget.presenter.EnterDetailPresenter;
import net.jakedev.openbudget.repository.BudgetRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Jake Laurie on 8/02/16.
 */
@Module
public class BudgetModule {

   private final Context mCtx;

    public BudgetModule(Context ctx) {
        mCtx = ctx;
    }

    @Provides @PerActivity
    EnterDetailPresenter provideEnterDetailPresenter(DataHelper dataHelper) {
        return new EnterDetailPresenter(dataHelper);
    }

    @Provides @PerActivity DailyBudgetPresenter provideDailyBudgetPresenter(BudgetRepository budgetRepository) {
        return new DailyBudgetPresenter(budgetRepository);
    }

    @Provides @PerActivity BudgetActivityPresenter provideBudgetActivityPresenter(DataHelper dataHelper) {
        return new BudgetActivityPresenter(dataHelper);
    }

    @Provides @PerActivity AddExpensePresenter provideAddExpensePresenter(DataHelper dataHelper) {
        return new AddExpensePresenter(dataHelper);
    }

}

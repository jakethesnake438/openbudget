package net.jakedev.openbudget.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Jake Laurie on 4/02/16.
 */
public class RLMBudget extends RealmObject {
    @PrimaryKey private String budgetID;
    @Required
    private Date startDate;
    @Required
    private Date endDate;
    private double totalBudgetAmount;
    private double originalDailyBudgetAmount;

    public double getTotalBudgetAmount() {
        return totalBudgetAmount;
    }

    public void setTotalBudgetAmount(double totalBudgetAmount) {
        this.totalBudgetAmount = totalBudgetAmount;
    }

    public String getBudgetID() {
        return budgetID;
    }

    public void setBudgetID(String budgetID) {
        this.budgetID = budgetID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getOriginalDailyBudgetAmount() {
        return originalDailyBudgetAmount;
    }

    public void setOriginalDailyBudgetAmount(double originalDailyBudgetAmount) {
        this.originalDailyBudgetAmount = originalDailyBudgetAmount;
    }

}

package net.jakedev.openbudget.di;

/**
 * Created by jake on 14/02/16.
 */
public interface HasComponent<C> {
    C getComponent();
}

package net.jakedev.openbudget.presenter;

import net.jakedev.openbudget.model.BudgetDay;
import net.jakedev.openbudget.model.realm.RLMBudgetDay;
import net.jakedev.openbudget.model.realm.RLMExpense;
import net.jakedev.openbudget.model.realm.RLMIncome;
import net.jakedev.openbudget.repository.BudgetRepository;
import net.jakedev.openbudget.util.BudgetUtil;

import javax.inject.Inject;

/**
 * Created by Jake Laurie on 5/02/16.
 */
public class DailyBudgetPresenter extends Presenter {

   public interface View {
       void setBudget(BudgetDay budget);
    }

    private BudgetRepository mBudgetRepository;
    private View mView;

    @Inject
    public DailyBudgetPresenter(BudgetRepository budgetRepository) {
        mBudgetRepository = budgetRepository;
    }

    public void setView(View view) {
        mView = view;
    }

    public void initialize () {
        updateViewData();
    }

    public String getCurrencyCode() {
        return "$";
        // TODO: implement currency code on RLMBudget object
    }

    public void updateViewData() {
        mView.setBudget(mBudgetRepository.retrieveBudgetForDate(BudgetUtil.getTodaysDateTimeless()));
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}

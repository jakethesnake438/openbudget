package net.jakedev.openbudget.view.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;

import net.jakedev.openbudget.R;
import net.jakedev.openbudget.di.HasComponent;
import net.jakedev.openbudget.di.component.BudgetComponent;
import net.jakedev.openbudget.presenter.AddExpensePresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.functions.Action1;

/**
 * Created by jake on 19/02/16.
 */
public class AddExpenseDialogFragment extends DialogFragment implements AddExpensePresenter.View {

    public interface AddExpenseIncomeListener {
        void dialogDismissed(boolean isCancel);
    }

    private static final String TAG = AddExpenseDialogFragment.class.getSimpleName();
    private static String TITLE_KEY = "title";
    private static String IS_EXPENSE_KEY = "is_expense";
    private boolean mIsExpense;
    private AddExpenseIncomeListener mAddExpenseListener;
    @Inject AddExpensePresenter mPresenter;

    @Bind(R.id.dialog_add)         Button addButton;
    @Bind(R.id.dialog_title)       TextView titleTextView;
    @Bind(R.id.dialog_value)       EditText valueText;
    @Bind(R.id.dialog_description) EditText descriptionText;

    public static AddExpenseDialogFragment newInstance(String title, boolean isExpense) {
        AddExpenseDialogFragment fragment = new AddExpenseDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TITLE_KEY, title);
        bundle.putBoolean(IS_EXPENSE_KEY, isExpense);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void initialize(String title, boolean isExpense) {
        titleTextView.setText(title);
        mIsExpense = isExpense;
    }

    public void setDialogListener(AddExpenseIncomeListener listener) {
        mAddExpenseListener = listener;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(BudgetComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.fragment_expense_dialog, container, false);
        ButterKnife.bind(this, view);
        mPresenter.setView(this);

        listenToInputs();

        Bundle args = getArguments();
        if(args != null) {
            initialize(args.getString(TITLE_KEY),
                    args.getBoolean(IS_EXPENSE_KEY));
        }
        return view;
    }

    @OnClick(R.id.dialog_cancel)
    public void dialogCancel() {
        dismissDialog(false);
    }

    public void dismissDialog(boolean expenseAdded) {
        mAddExpenseListener.dialogDismissed(expenseAdded);
        dismiss();
    }

    @OnClick(R.id.dialog_add)
    public void addExpenseOrIncome() {
        mPresenter.addExpenseOrIncome(valueText.getText().toString(),
                descriptionText.getText().toString(),
                mIsExpense);
        dismissDialog(true);
    }

    private void listenToInputs() {
        Observable<TextViewAfterTextChangeEvent> observable = RxTextView.afterTextChangeEvents(valueText);
        observable.subscribe(new Action1<TextViewAfterTextChangeEvent>() {
            @Override
            public void call(TextViewAfterTextChangeEvent afterTextChangeEvent) {
                addButton.setEnabled(mPresenter.incomeExpenseValueIsValid(afterTextChangeEvent.editable().toString()));
            }
        });
    }

    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

}

package net.jakedev.openbudget.data;

import android.content.Context;

import io.realm.DynamicRealm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by Jake Laurie on 19/02/16.
 */
public class Migration implements RealmMigration {

    public static final String REALM_CONFIG_NAME = "realm_config_0";
    public static final int REALM_SCHEMA_VERSION = 0;

    public static RealmConfiguration getRealmConfiguration(Context ctx) {
        RealmConfiguration config = new RealmConfiguration.Builder(ctx)
                .name(REALM_CONFIG_NAME)
                .schemaVersion(REALM_SCHEMA_VERSION)
                .migration(new Migration())
                .build();

        return config;
    }

    @Override
    public void migrate(final DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        if (oldVersion == 0) {
            //TODO setup migration!
            oldVersion ++;
        }
    }
}

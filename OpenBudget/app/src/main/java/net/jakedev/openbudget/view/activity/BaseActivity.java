package net.jakedev.openbudget.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import net.jakedev.openbudget.AndroidApplication;
import net.jakedev.openbudget.di.module.ActivityModule;
import net.jakedev.openbudget.di.component.ApplicationComponent;

/**
 * Created by Jake Laurie on 31/01/16.
 */

public class BaseActivity extends AppCompatActivity {

    public interface OnBackPressedHandler {
        boolean handlesBackPress();
        void onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getApplicationComponent().inject(this);
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((AndroidApplication)getApplication()).getApplicationComponent();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    private OnBackPressedHandler backPressHandler;

    public void setSelectedFragment(OnBackPressedHandler selectedFragment) {
        this.backPressHandler = selectedFragment;
    }

    public void hideActionbarTitle(boolean hide) {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(!hide);
        }
    }

    @Override
    public void onBackPressed() {
        if(backPressHandler != null
                && backPressHandler.handlesBackPress()) {
            backPressHandler.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}

package net.jakedev.openbudget.repository;

import net.jakedev.openbudget.data.DataHelper;
import net.jakedev.openbudget.model.BudgetDay;
import net.jakedev.openbudget.model.realm.IncomeExpenseWrapper;
import net.jakedev.openbudget.model.realm.RLMBudgetDay;
import net.jakedev.openbudget.model.realm.RLMExpense;
import net.jakedev.openbudget.model.realm.RLMIncome;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by jake on 24/02/16.
 */
public class BudgetRepository {

    private DataHelper mDataHelper;

    @Inject
    public BudgetRepository(DataHelper dataHelper) {
        mDataHelper = dataHelper;
    }

    public BudgetDay retrieveBudgetForDate(Date date) {
        RLMBudgetDay budgetDay =  mDataHelper.getCurrentBudgetForDate(date);
        return new BudgetDay(budgetDay.getBudgetSpent(),
                budgetDay.getBudgetAmount(),
                getIncomeAndExpenseList(budgetDay));
    }

    public void addExpenseForDate(RLMExpense RLMExpense, Date date) {
        mDataHelper.addExpenseForDay(RLMExpense, date);
    }

    public void addIncomeForDate(RLMIncome RLMIncome, Date date) {
        mDataHelper.addIncomeForDay(RLMIncome, date);
    }

    private List<IncomeExpenseWrapper> getIncomeAndExpenseList(RLMBudgetDay budgetDay) {
        List<IncomeExpenseWrapper> incomeExpenseList = new ArrayList<>();
        for (RLMIncome RLMIncome : budgetDay.getRLMIncomeList()) {
            incomeExpenseList.add(new IncomeExpenseWrapper(RLMIncome));
        }

        for(RLMExpense RLMExpense : budgetDay.getRLMExpenseList()) {
            incomeExpenseList.add(new IncomeExpenseWrapper(RLMExpense));
        }

        return incomeExpenseList;
    }
}

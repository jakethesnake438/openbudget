package net.jakedev.openbudget.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Jake Laurie on 8/02/16.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {}

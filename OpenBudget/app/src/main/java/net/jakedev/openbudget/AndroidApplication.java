package net.jakedev.openbudget;

import android.app.Application;

import net.jakedev.openbudget.di.component.ApplicationComponent;
import net.jakedev.openbudget.di.module.ApplicationModule;
import net.jakedev.openbudget.di.component.DaggerApplicationComponent;
import net.jakedev.openbudget.di.module.DataModule;

/**
 * Created by Jake Laurie on 8/02/16.
 */
public class AndroidApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
    //TODO: add leak canary here
    //TODO: add Crashlytics here
}

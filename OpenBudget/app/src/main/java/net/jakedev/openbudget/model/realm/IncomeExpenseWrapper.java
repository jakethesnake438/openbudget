package net.jakedev.openbudget.model.realm;

import io.realm.RealmObject;

/**
 * Created by jake on 21/02/16.
 */
public class IncomeExpenseWrapper {
    private double priceValue;
    private String description;

    /**
     * Getting around RealmObject's no-custom-methods limitation
     * @param object should be a {@link RLMExpense} or {@link RLMIncome} object
     */

    public IncomeExpenseWrapper(RealmObject object) {
        if(object instanceof RLMIncome) {
            initWithIncome((RLMIncome) object);
        } else if(object instanceof RLMExpense) {
            initWithExpense((RLMExpense) object);
        } else {
            throw new IllegalArgumentException("Object was neither"
                    + RLMIncome.class.getSimpleName()
                    + "nor"
                    + RLMExpense.class.getSimpleName());
        }
    }

    private void initWithExpense(RLMExpense RLMExpense) {
        this.priceValue = - RLMExpense.getPriceValue();
        this.description = RLMExpense.getDescription();
    }

    private void initWithIncome(RLMIncome RLMIncome) {
        this.priceValue = RLMIncome.getPriceValue();
        this.description = RLMIncome.getDescription();
    }

    public String getDescription() {
        return description;
    }

    public double getPriceValue() {
        return priceValue;
    }

}

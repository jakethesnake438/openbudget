package net.jakedev.openbudget.data;

import android.content.Context;

import net.jakedev.openbudget.model.realm.RLMBudget;
import net.jakedev.openbudget.model.realm.RLMBudgetConfig;
import net.jakedev.openbudget.model.realm.RLMBudgetDay;
import net.jakedev.openbudget.model.realm.RLMExpense;
import net.jakedev.openbudget.model.realm.RLMIncome;
import net.jakedev.openbudget.util.BudgetUtil;
import net.jakedev.openbudget.util.OBLog;

import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by Jake Laurie on 3/02/16.
 */
public class DataHelper {

    private static final String LOGTAG = DataHelper.class.getSimpleName();
    Realm mRealm;

    public DataHelper(Context ctx) {
        mRealm = Realm.getInstance(Migration.getRealmConfiguration(ctx));
    }

    public void setupNewBudget(Date startDate,
                               Date endDate,
                               double budgetTotal) {

        RLMBudget rlmBudget = new RLMBudget();
        rlmBudget.setBudgetID(createBudgetUID());
        rlmBudget.setStartDate(startDate);
        rlmBudget.setEndDate(endDate);
        rlmBudget.setTotalBudgetAmount(budgetTotal);
        rlmBudget.setOriginalDailyBudgetAmount(BudgetUtil.calculateOriginalBudgetAmount(startDate,
                endDate,
                budgetTotal));

        mRealm.beginTransaction();
        mRealm.copyToRealm(rlmBudget);
        mRealm.commitTransaction();

        updateCurrentConfig(rlmBudget.getBudgetID());
        OBLog.v(LOGTAG, "New budget created for +" + rlmBudget.getBudgetID());
    }

    public RLMBudget getBudget() {
        RLMBudgetConfig config = getCurrentConfig();
        return mRealm.where(RLMBudget.class)
                .equalTo("budgetID", config.getCurrentBudgetID()).findFirst();
    }

    public RLMBudgetDay getCurrentBudgetForDate(Date date) {
        String budgetID = getCurrentConfig().getCurrentBudgetID();

        RLMBudgetDay budgetDay = (budgetID != null)
                ? mRealm.where(RLMBudgetDay.class)
                .equalTo("dayLong", date.getTime())
                .equalTo("budgetID", budgetID)
                .findFirst()
                : createNewBudgetForDate(date);

        if(budgetDay == null) {
            OBLog.d(LOGTAG, "Daily budget instance created for " + date.toString());
            budgetDay = createNewBudgetForDate(date);
        }
        setBudgetForDate(budgetDay);
        return budgetDay;
    }

    private RLMBudgetDay createNewBudgetForDate(Date date) {
        RLMBudget currentBudgetConfig = getBudget();
        RLMBudgetDay budget = new RLMBudgetDay();

        budget.setBudgetID(currentBudgetConfig.getBudgetID());
        budget.setBudgetAmount(BudgetUtil.getBudgetAmountForDate(date,
                currentBudgetConfig.getEndDate(),
                currentBudgetConfig.getOriginalDailyBudgetAmount(), //WTF IS THE ORIGINAL BUDGET VAL
                currentBudgetConfig.getTotalBudgetAmount()));
        budget.setBudgetSpent(0f);
        budget.setDay(date);

        return budget;
    }

    public void setBudgetForDate(RLMBudgetDay budget) {
        mRealm.beginTransaction();
        mRealm.copyToRealmOrUpdate(budget);
        mRealm.commitTransaction();
    }

    public void updateCurrentConfig(String budgetID) {
        RLMBudgetConfig config = getCurrentConfig();
        if(config == null) {
            config = new RLMBudgetConfig();
        }

        mRealm.beginTransaction();
        config.setCurrentBudgetID(budgetID);
        mRealm.copyToRealmOrUpdate(config);
        mRealm.commitTransaction();
    }

    public RLMBudgetConfig getCurrentConfig() {
        RealmResults<RLMBudgetConfig> config = mRealm.allObjects(RLMBudgetConfig.class);
        return config.size() == 0 ? null : config.first();
    }

    public void addExpenseForDay(RLMExpense expense, Date day) {
        RLMBudgetDay budget = getCurrentBudgetForDate(day);
        mRealm.beginTransaction();

        RealmList<RLMExpense> RLMExpenseList = budget.getRLMExpenseList();
        budget.setBudgetSpent(budget.getBudgetSpent() + expense.getPriceValue());
        RLMExpenseList.add(expense);

        mRealm.commitTransaction();
    }

    public void addIncomeForDay(RLMIncome income, Date day) {
        RLMBudgetDay budget = getCurrentBudgetForDate(day);

        mRealm.beginTransaction();

        RealmList<RLMIncome> RLMIncomeList = budget.getRLMIncomeList();
        budget.setBudgetSpent(budget.getBudgetSpent() - income.getPriceValue());
        RLMIncomeList.add(income);

        mRealm.commitTransaction();
    }

    private static String createBudgetUID() {
        return UUID.randomUUID().toString();
    }

}

package net.jakedev.openbudget.util;

import android.util.Log;

/**
 * Created by Jake Laurie on 3/02/16.
 */
public class OBLog {

    public static int v(String tag, String msg) {
        if(!OBConfig.IS_LIVE) {
            return Log.v(tag, msg);
        }
        return -1;
    }

    public static int v(String tag, String msg, Throwable tr) {
        if(!OBConfig.IS_LIVE) {
            return Log.v(tag, msg, tr);
        }
        return -1;
    }

    public static int d(String tag, String msg) {
        if(!OBConfig.IS_LIVE) {
            return Log.d(tag, msg);
        }
        return -1;
    }

    public static int d(String tag, String msg, Throwable tr) {
        if(!OBConfig.IS_LIVE) {
            return Log.d(tag, msg, tr);
        }
        return -1;
    }

    public static int i(String tag, String msg) {
        if(!OBConfig.IS_LIVE) {
            return Log.i(tag, msg);
        }
        return -1;
    }

    public static int i(String tag, String msg, Throwable tr) {
        if(!OBConfig.IS_LIVE) {
            return Log.i(tag, msg, tr);
        }
        return -1;
    }

    public static int w(String tag, String msg) {
        if(!OBConfig.IS_LIVE) {
            return Log.w(tag, msg);
        }
        return -1;
    }

    public static int w(String tag, String msg, Throwable tr) {
        if(!OBConfig.IS_LIVE) {
            return Log.w(tag, msg, tr);
        }
        return -1;
    }

    public static int w(String tag, Throwable tr) {
        if(!OBConfig.IS_LIVE) {
            return Log.w(tag, tr);
        }
        return -1;
    }

    public static int e(String tag, String msg) {
        return Log.e(tag, msg);
    }

    public static int e(String tag, String msg, Throwable tr) {
        return Log.e(tag, msg, tr);
    }
}

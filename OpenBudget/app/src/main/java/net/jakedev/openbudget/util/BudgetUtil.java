package net.jakedev.openbudget.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jake Laurie on 5/02/16.
 */
public class BudgetUtil {

    /**
     * Returns allowed spending for the day,
     * Behaviour is that what is not spent previously is added to todays budget
     * @param date Today's date
     * @param budgetEndDate The end date of the budget
     * @param originalDailyAmount The original budget amount, (total budget days / total budget value)
     * @param budgetAmountRemaining How much we have left to spend for the budget duration
     * @return amount for day
     */
    public static double getBudgetAmountForDate(Date date,
                                                Date budgetEndDate,
                                                double originalDailyAmount,
                                                double budgetAmountRemaining) {
        long daysRemaining = getDaysRemaining(date, budgetEndDate);
        return originalDailyAmount
                + ((budgetAmountRemaining / daysRemaining) - originalDailyAmount)
                * daysRemaining;
    }

    /**
     * @param startDate The date that the budget should start
     * @param endDate The date that the budget should end
     * @param totalBudgetAmount The amount that should be spent between the dates
     * @return the default amount that should be spent daily
     */
    public static double calculateOriginalBudgetAmount(Date startDate, Date endDate, double totalBudgetAmount) {
        return totalBudgetAmount / getDaysRemaining(startDate, endDate);
    }

    /**
     * @return number of calendar days between
     * @param start
     * and
     * @param end
     */
    public static long getDaysRemaining(Date start, Date end) {
        return TimeUnit.DAYS.convert(end.getTime() - start.getTime(),
                TimeUnit.MILLISECONDS);
    }

    /**
     * @return todays date at 0:00
     */
    public static Date getTodaysDateTimeless() {
        Calendar date = new GregorianCalendar();
        date.setTime(new Date());
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        return date.getTime();
    }
}

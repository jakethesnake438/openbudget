package net.jakedev.openbudget.presenter;

import net.jakedev.openbudget.data.DataHelper;
import net.jakedev.openbudget.model.realm.RLMExpense;
import net.jakedev.openbudget.model.realm.RLMIncome;
import net.jakedev.openbudget.util.BudgetUtil;
import net.jakedev.openbudget.util.OBStringUtils;
import net.jakedev.openbudget.util.OBLog;

import javax.inject.Inject;

/**
 * Created by jakelaurie on 1/04/16.
 */
public class AddExpensePresenter extends Presenter {

    public static interface View {
    }

    private View mView;
    private DataHelper mDataHelper;
    private boolean mViewIsDestroyed;

    @Inject
    public AddExpensePresenter(DataHelper dataHelper) {
        mDataHelper = dataHelper;
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void pause() {
        // Do nothing
    }

    @Override
    public void destroy() {
        mView = null;
        mViewIsDestroyed = true;
    }

    public boolean incomeExpenseValueIsValid(String value) {
        boolean isValid;

        try {
            value = OBStringUtils.removeAllNonNumeric(value);
            isValid = Double.parseDouble(value) > 0.f;
        } catch (NumberFormatException e) {
            isValid = false;
        }

        return isValid;
    }

    public double getValueFromString(String str) {
        str = OBStringUtils.removeAllNonNumeric(str);
        return Double.parseDouble(str);
    }

    public void addExpenseOrIncome(String value, String description, boolean isExpense) {
        double val = getValueFromString(value);

        if(isExpense) {
            mDataHelper.addExpenseForDay(new RLMExpense(val, description), BudgetUtil.getTodaysDateTimeless());
        } else {
            mDataHelper.addIncomeForDay(new RLMIncome(val, description), BudgetUtil.getTodaysDateTimeless());
        }
    }

    public void setView(View v) {
        mView = v;
    }
}

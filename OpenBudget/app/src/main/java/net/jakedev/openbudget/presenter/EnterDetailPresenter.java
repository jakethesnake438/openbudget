package net.jakedev.openbudget.presenter;

import net.jakedev.openbudget.data.DataHelper;
import net.jakedev.openbudget.util.BudgetUtil;
import net.jakedev.openbudget.util.OBLog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;

/**
 * Created by Jake Laurie on 2/02/16.
 */
public class EnterDetailPresenter extends Presenter {

    private DataHelper mDataHelper;
    private static final String LOG_TAG = EnterDetailPresenter.class.getSimpleName();

    private static String[] periodSpinnerValues
            = {"Days", "Weeks", "Months"};

    private String[] currencyCodeSpinnerValues;

    @Inject
    public EnterDetailPresenter(DataHelper dataHelper) {
        mDataHelper = dataHelper;
    }

    public String[] getPeriodSpinnerValues() {
        return periodSpinnerValues;
    }

    public String[] getCurrencyCodeSpinnerValues() {
        if(currencyCodeSpinnerValues == null) {
            currencyCodeSpinnerValues = getAllCurrencySymbols();
        }
        return currencyCodeSpinnerValues;
    }

    public boolean validate(String periodText, String amountText) {
        try {
            Integer.parseInt(amountText);
            Integer.parseInt(periodText);
        } catch (NumberFormatException
                | IndexOutOfBoundsException
                | NullPointerException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void setupBudgetInformation(String periodText,
                                          String amountText,
                                          int periodSpinnerPos) {

        Date endDate = BudgetUtil.getTodaysDateTimeless();
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        int offset = Integer.parseInt(periodText);
        switch (periodSpinnerValues[periodSpinnerPos]) {
            case "DAYS":
                cal.add(Calendar.DATE, offset);
                break;
            case "WEEKS":
                cal.add(Calendar.WEEK_OF_MONTH, offset);
                break;
            case "MONTHS":
                cal.add(Calendar.MONTH, offset);
                break;
            default:
                cal.add(Calendar.DAY_OF_WEEK_IN_MONTH, offset);
                OBLog.d(LOG_TAG, "Unknown date increment type");
            break;

        }

        endDate = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        OBLog.d(LOG_TAG, "RLMBudget will end " + sdf.format(endDate));

        mDataHelper.setupNewBudget(BudgetUtil.getTodaysDateTimeless(),
                endDate,
                Integer.parseInt(amountText));
    }

    public  String[] getAllCurrencySymbols() {
        Set<String> currencies = new HashSet<>();
        Locale[] locales = Locale.getAvailableLocales();

        for(Locale locale : locales) {
            try {
                currencies.add(Currency.getInstance(locale).getSymbol());
            } catch (Exception exc) {
                exc.printStackTrace();
                OBLog.d(LOG_TAG, "Currency locale issue");
            }
        }

        List<String> prettyCurrencies = new ArrayList<>();
        for(String currency : currencies) {
            if(currency.length() < 3) {
                prettyCurrencies.add(currency);
            }
        }

        return prettyCurrencies.toArray(new String[prettyCurrencies.size()]);
    }
}

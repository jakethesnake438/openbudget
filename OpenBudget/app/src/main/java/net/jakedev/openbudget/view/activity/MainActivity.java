package net.jakedev.openbudget.view.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import net.jakedev.openbudget.R;
import net.jakedev.openbudget.di.component.BudgetComponent;
import net.jakedev.openbudget.di.module.BudgetModule;
import net.jakedev.openbudget.di.component.DaggerBudgetComponent;
import net.jakedev.openbudget.di.HasComponent;
import net.jakedev.openbudget.di.module.DataModule;
import net.jakedev.openbudget.presenter.BudgetActivityPresenter;
import net.jakedev.openbudget.view.fragment.DailyBudgetFragment;
import net.jakedev.openbudget.view.fragment.EnterDetailFragment;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements HasComponent<BudgetComponent>, BudgetActivityPresenter.View {

    @Bind(R.id.toolbar) public Toolbar mToolbar;

    @Inject public BudgetActivityPresenter mPresenter;
    private BudgetComponent budgetComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.initializeInjector();
        getComponent().inject(this);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        if (savedInstanceState == null) {
            mPresenter.setView(this);
            mPresenter.initialize();
        }
    }

    public void presentDailyBudgetView() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container,
                        new DailyBudgetFragment())
                .commit();
    }

    private void initializeInjector() {
        this.budgetComponent = DaggerBudgetComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .budgetModule(new BudgetModule(getApplicationContext()))
                .build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override public BudgetComponent getComponent() {
        return budgetComponent;
    }

    @Override
    public void noBudgetAvailable() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container,
                        new EnterDetailFragment())
                .commit();
    }

    @Override
    public void budgetAvailable() {
        presentDailyBudgetView();
    }
}
